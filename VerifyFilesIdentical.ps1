﻿#Open powershell.exe (either as user or admin). Then, use the following command.
#
#Compare-Object "$(Get-Content $PATH1)" "$(Get-Content $PATH2)"
#You can set $PATH1 and $PATH2 to your file paths and copy paste the command above.
#
#If it returns nothing, the files are identical in content, which is what I believe you want. 
#Note that this command does not check for identical permissions, identical modified date, etc. To compare identical modified date and mode, use the following command.
#
#Compare-Object "$(Get-ItemProperty $PATH1 | Select-Object Mode, LastWriteTime)" "$(Get-ItemProperty $PATH2 | Select-Object Mode, LastWriteTime)"
#To compare the permission, use the following command.
#
#Compare-Object "$(Get-Acl $PATH1).Access" "$(Get-Acl $PATH2).Access"
#Again, you can set $PATH1 and $PATH2 and just copy and paste the commands above.
#
#In all three commands, if it returns something, you know that they are not the same. On the other hand, 
#if it returns nothing, you know that they are exactly the same in content (1st command), last write time and mode (2nd command), and permissions (3rd command).

#Hashing Algorith
$Algorithm = 'SHA256'

#Source Path
$Path = "C:\temp"
$SourcePath = (Resolve-Path -Path $Path).Path
write-host "Path Directory is:" $Path
$Path | Get-Member
write-host "Source Directory is:" $SourcePath
#$SourcePath | Get-Member

#Destniation Path
$Destination = "C:\temp\"
Write-Host "Writing Destination Folder:" $Destination
#$Destination | Get-Member

