#Created 18 October 2018
#This script is designed to copy one folder to a new folder on a different cloud environment.
#This commmand can be put  at the top of every single script so tha tyou don't have to check $?.
#This makes the code vastly simpler and more reliable.
#If there are situations where it is necessary to disable this behavior,
#Either use 'catch' to get the error or pass a setting to a particular function using the common -ErrorAction.
#In this case, there is a need to stop the process on the first error, catch the error, and then log it.

$ErrorActionPreference = 'Stop'
$TimeStart = get-date

#Otherways to handle errors
#https://community.spiceworks.com/how_to/3016-error-handling-and-logging-in-powershell

#Define the source drive adn Name of Computer hosting Share:
$SourceDrive = "C:\temp"
$ComputerShare ="C:\temp1"

#Define Test_ConnectionQuietFast function
Function Test-ConnectionQuietFast {

    [CmdletBinding()]
    param(
    [String]$ComputerName,
    [int]$Count = 1,
    [int]$Delay = 500
    )

    for($I = 1; $I -lt $Count + 1 ; $i++)
    {
        Write-Verbose "Ping Computer: $ComputerName, $I try with delay $Delay milliseconds"

        # Test the connection quiet to the computer with one ping
        If (Test-Connection -ComputerName $ComputerName -Quiet -Count 1)
        {
            # Computer can be contacted, return $True
            Write-Verbose "Computer: $ComputerName is alive! With $I ping tries and a delay of $Delay milliseconds"
            return $True
        }

        # delay between each pings
        Start-Sleep -Milliseconds $Delay
    }

    # Computer cannot be contacted, return $False
    Write-Verbose "Computer: $ComputerName cannot be contacted! With $Count ping tries and a delay of $Delay milliseconds"
    $False
}

if (Test-ConnectionQuietFast -ComputerName "192.168.1.1") {
Write-Host "The script was able to ping the remote share."
} else {
write-host "The script was not able to ping the remote (source) share."
exit
}


#Define the destination Drive
$DestinationDrive = "C:\Temp1"

#Define Directory for Error Files (to be on local drive)
$ErrorMessagesDirectory = "C:\Temp1\Logs\"
$ErrorMessagesFile = "C:\Temp1\Logs\FileCopyLogs.txt"

#Check if the ErrorMessage file exist, if not create one.
#$FileExists = Test-Path $ErrorMessagesFile
If (Test-Path $ErrorMessagesFile)
{Write-Host "Just checked that error log exists."}
Else {
New-Item -Path $ErrorMessagesDirectory -name FileCopyLogs.txt -ItemType File -Force
"This file was created on $TimeStart" | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII
}

If(!(Test-Path $DestinationDrive)) {
Write-Host "The Destination Drive share is not available."
exit
}

Write-host "Starting the process to collect the Source Files."
Get-ChildItem -Path $SourceDrive -Force -Recurse

Write-host "Starting the process to collect the Destination Files."
Get-ChildItem -Path $DestinationDrive -Force -Recurse

#The -Force command lists only the directly contained items, much like using Cmd.exe's DIR command or ls in a UNIX shell.
#The -Recurse command shows all the contained items. (This can take an extremely long time to complete.)

#Create an array of Files on SourceDrive
$FileSource = @(Get-ChildItem $SourceDrive)


if ($FileSource.length -eq 0) {
write-host "No files in the source directory."
} else {
write-host "There are files in the source array."
}


#Source path to be used by RoboCopy
set sourcepathRoboCopy="Y:\"

#Destination path to be used by RoboCopy
set destinationpathRoboCopy="C:\Temp1"


#Create an array of Files on DestinationDrive
$FileDestination = @(Get-ChildItem $DestinationDrive)
Write-host "Printing the Destination File Names."
Write-Host $($FileDestination)

#Print the Destination Array
$DestinationArray = @( Get-ChildItem $DestinationDrive -Recurse)
Write-Host $DestinationArray.count



if ($FileDestination.length -eq 0) {
write-host "There are no files in the Destination directory."
Write-host "Copying the files to $DestinationDrive."

  #If no destination files exist; Copy the source drive to the destination drive.
  #It is not possible to use Copy-Item as this can not write to a drive or UNC name.

robocopy $SourceDrive $DestinationDrive\ /MIR /XX /XA:SH /Log:$ErrorMessagesFile
} else {
write-host "There are files in the Destination array, starting to mirror the site."

robocopy $SourceDrive $DestinationDrive\ /MIR /XX /XA:SH /Log+:$ErrorMessagesFile
}



write-host "All files copied from source to destination."

#While Write-Output and Write-Host seemed to achieve the same result in our basic initial example they are actually working very differently.
#Write-Host does not output data to PowerShell Objectflow Engine but rather, as the name implies, writes directly to the host.
#It send nothing to the PowerShell engine to be forwarded to commands later in the pipeline.
#https://www.itprotoday.com/management-mobility/write-output-or-write-host-powershell
