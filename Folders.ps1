#This code will check that the high level directories exist in the destination directory or create it.

#Source path to be used
$SourceDirectory = "C:/Temp/"
$DestinationDirectory = "C:/Temp1/"

$FolderSource = @(Get-ChildItem -Directory $SourceDirectory)

write-host	"The number of Source Folders in the Top level directory = " $FolderSource.count

foreach ($Folder in $FolderSource) {
#Combining the Source Directory starting point with the current folder
write-host "Source Location =" $SourceDirectory$Folder
$Destination = $DestinationDirectory+$Folder
Write-Host "Destination Location =" $Destination
$ValidPath = Test-Path -Path $Destination
Write-Host "The ValidPath variable is" $ValidPath

If ($ValidPath -eq $False){

    New-Item -Path $Destination -ItemType directory
    Copy-Item -Path $SourceDirectory+$Folder -Destination $Destination -force
}

Else {
      Write-Host "No need to create Source Directory into Destination DIrectory."
     }
}

#Get the files in the Source Directory
$FileSource = @(Get-ChildItem -Path $SourceDirectory -Force -Recurse)

foreach ($item in $FileSource)
{
    $item.name
    write-host "Item Name =" $item.name
}
$FileDestination = @(Get-ChildItem -Path $DestinationDirectory -Force -Recurse)

$FileCompare = @(Compare-Object -ReferenceObject $FileSource -DifferenceObject $FileDestination)

foreach ($FileC in $FileCompare) {
write-host $FileC.InputObject
#$filepath = ((Get-ChildItem -Path $DestinationDirectory -Force -Recurse) | where_object {$item -contains $_.BaseName} | Select-object FullName)
#write-host "The files path name is" $filepath

if ($FileC.SideIndicator  -eq "<=") {
Write-Host $FileC.InputObject "exists in the source directory only."
Write-Host "Need to copy "$FileC.InputObject" to Destination Directory" $DestinationDirectory
}

elseif ($FileC.SideIndicator  -eq "==") {
write-host "The file exists in both source and destination directories."
}
Elseif ($FileC.SideIndicator  -eq "=>") {
write-host "The file exists in the destination directory only."
}
else {
Write-Host "Something went wrong: not able to compare!"
}
}

