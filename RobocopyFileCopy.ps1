#Copy all files from sourcepath to destinationpath using Robocopy
#20181018 Convert script to copy files from a NAS share to a cloud share
#20130411 Replaced /E with /MIR to remove older files from destination
#20130211 Updated date format of log file to YYYY-MM-DD
#20130118 Limey https://community.spiceworks.com/topic/390498-running-xcopy-with-task-scheduler
#Original file by Adam Rush, http://www.virtuallyimpossible.co.uk
#
#Switches:
#(E)mpty folders included in copy
#(R)etry each copy up to 0 times
#(W)ait 0 seconds between attempts
#(LOG) creates log file
#(NP) do not include progress txt in logfile; this keeps filesize down
#(MIR)rors a directory tree
#(V)erbose logging
#COPYALL Copy all file info
#ETA show Estimated Time of Arrival of copied files

#Powershell Execution.
#When running script using powershell, you will need to temporarily pause the restriction when running the script.
#https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/set-executionpolicy?view=powershell-6
#e.g: Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass
#
#This command sets an execution policy of Bypass for only the current PowerShell session.
#This execution policy is saved in the PSExecutionPolicyPreference environment variable ($env:PSExecutionPolicyPreference), so it does not affect the value in the registry.
#The variable and its value are deleted when the current session is closed.
#Bypass. Nothing is blocked and there are no warnings or prompts.

#Network drive mapping
net use w: \\xx.xxx.xxx.xxx\xxxxxxxx

# The destination directory is as follows: "C:\temp"

#Source path
set sourcepath=C:\temp

#Destination path
set destinationpath=C:\temp1

#Log path
set logpath="C:\temp\Downloads\RoboCopy_Logs"

Include format yyyy-mm-dd#hh-mm-ss.ms in log filename
set filename=Backup_Job_%date:~-4,4%-%date:~-10,2%-%date:~-7,2%#%time::=-%.txt

#Run command
robocopy "%sourcepath%" "%destinationpath%" /MIR /V /ETA /R:0 /W:0 /COPYALL /LOG:"%logpath%%filename%" /NP

#Email
#"c:\Program Files\blat307\full\blat.exe" -server server.domain.com -f sender@domain.com -t recipient@domain.com -subject "[Success] %filename%" -body "%filename% Successful"

#Sample using RoboCopy
#C:\Windows\System32\Robocopy.exe "d:\data\SharedFiles\Dropbox" "\\otherserver\Backups\DropboxBackup" /s /e /r:0 /w:0 /MOT:5
#
#When running this script from Powershell
#powershell Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass ./copy_files_robocopy.ps1
