#Created 06 November 2018
#This script is designed to manipulate an array with folder and directory information

#Source path to be used
$SourceDirectory = "C:\Temp\"

#DirectoryPath to be used
$DestinationDirectory = "C:\Temp1\"

#Creating the collection of files
Write-host "Starting the process to collect the Source Files."
$FileSource = @(Get-ChildItem -Path $SourceDirectory -Force -Recurse)
$FolderSource = @(Get-ChildItem -Path $SourceDirectory)

$FileDestination = @(Get-ChildItem -Path $DestinationDirectory -Force -Recurse)

#Lets confirm that the variable is an array.
if (($FileSource) -is [array]) {
Write-Host ""
Write-host "FileSource is an array"
Write-Host "Filesource variable is of type $($FileSource).GetType();"
write-host ""
}
Else {
Write-Host "FileSource variable is not an array"
}


#Count the number of objects in the array
write-host $FileSource.count

#Return the length, last access time from within a statement list.
foreach ($file in $FileSource)
{
	if ($file.length -gt 1KB)
	{
		write-host $file
		write-host $file.length
		write-host $file.lastaccesstime
	}
}

#A foreach loop that incrementally increases an external variable.
$i=0
foreach ($file in $FileSource) {
    if ($file.Length -gt 0KB) {
        Write-Host $file "File size:" ($file.Length / 1024).ToString("F0") KB
        $i=$i + 1
        }
}

if ($i -ne 0) {
    Write-Host
    Write-Host $i "file(s) over 0KB in the current directory."
}
else {
    Write-Host "No files greater than 0KB in the current directory."
}

#Get the top level folders in the root directory
#$j = 0
#$FolderSource.Count
foreach ($folder in $FolderSource) {
    Write-Host "Top Level directory folder is:" $folder "and was last accessed: " ($folder.lastaccesstime)
    #Create the first sub-array that will be used to focus on one of the root directories.
    if (!($folder | Out-String) -ne "GO_TO_MARKET") { 
        Write-Host "This is the $folder folder"
        $Market = Out-String -InputObject $folder
        write-host "This is the" $Market "folder"
        Write-Host $Market
    }    
}

#Create array of just filename with associated full qualified path name
foreach ($item in $FileSource)
{
    $item.name
    write-host "Item Name =" $item.name
}


#For each file in FileSource check if it exists in FileDestination.
write-host "Comparing the two arrays for similarities."
Compare-Object -ReferenceObject $FileSource -DifferenceObject $FileDestination -IncludeEqual -ExcludeDifferent
write-host "Comparing the two arrays for differences."
Compare-Object -ReferenceObject $FileSource -DifferenceObject $FileDestination
$Arraytemp = @()
$Arraytemp = $FileDestination | Where {$FileSource -NotContains $_}
Write-Host "This is the content of the `$Arraytemp:"  
Write-host $Arraytemp

#If FileSource is in FileDestination is the fileSource last time of access greater than the FileSource last time of access?
#If not then copy FileSource to FileDestination.

#Is the output of $FileSource piped to Out-Sting (A powershell cmdlet) an array
($FileSource | Out-String) -is [System.Array]

if (($FileSource | Out-String) -isnot [System.Array]) {
Write-Host "The output of Out-String is not an array"
}
Else {
Write-Host "The output of the Out-String is an array!"
}

