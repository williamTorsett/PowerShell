#Created 8 December 2018
#This script is designed to check folders are available then copy files from source to destination.
#The copy from source to destination will ensure that only the latest newest version of file will be copied.
#If the destination drive has a newer file, then thsi will be copies to source.
#This commmand can be put  at the top of every single script so that you don't have to check $?.

$ErrorActionPreference = 'Stop'
#This makes the code vastly simpler and more reliable.
clear

#Setting the key variables.
$SourceDirectory = "C:\Temp"
$DestinationDirectory = "C:\Temp1"
set-location $SourceDirectory
$ComputerNameToPing = "127.0.0.1"
$TimeStart = get-date

#Define Directory for Error Files (to be on local drive)
$ErrorMessagesDirectory = "C:\Temp\Logs\"
$ErrorMessagesFile = "C:\Temp\Logs\FileCopyLogs.txt"

#Define all functions here:
#Define Test_ConnectionQuietFast function
Function Test-ConnectionQuietFast {

    [CmdletBinding()]
    param(
    [String]$ComputerName,
    [int]$Count = 1,
    [int]$Delay = 500
    )

    for($I = 1; $I -lt $Count + 1 ; $i++)
    {
        Write-Verbose "Ping Computer: $ComputerName, $I try with delay $Delay milliseconds"

        # Test the connection quiet to the computer with one ping
        If (Test-Connection -ComputerName $ComputerName -Quiet -Count 1)
        {
            # Computer can be contacted, return $True
            Write-Verbose "Computer: $ComputerName is alive! With $I ping tries and a delay of $Delay milliseconds"
            return $True
        }

        # delay between each pings
        Start-Sleep -Milliseconds $Delay
    }

    # Computer cannot be contacted, return $False
    Write-Verbose "Computer: $ComputerName cannot be contacted! With $Count ping tries and a delay of $Delay milliseconds"
    $False
}

#Check to see if remote drive is avilable.

if (Test-ConnectionQuietFast -ComputerName $ComputerNameToPing) {
"The script was initiated at "  + $TimeStart | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII
"The script was able to ping the remote share." | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII
} else {
"The script was not able to ping the remote (source) share." | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII
exit
}

#Remote drive is available, now check error message directory exists.
#Check if the ErrorMessage file exist, if not create one.
If (Test-Path $ErrorMessagesFile)
{"Just checked that error log exists." | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII}
Else {
New-Item -Path $ErrorMessagesDirectory -name FileCopyLogs.txt -ItemType File -Force
"This file was created on " + $TimeStart | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII
}

If(!(Test-Path $ErrorMessagesDirectory)) {
"The Error Message folder is not available." | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII
exit
}

write-Host "Starting the process to collect the Source Files."
"Starting the process to collect the Source Files." | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII

$FileSources = @()
$FileSources = @((Get-ChildItem -Path $SourceDirectory -Force -Recurse) | Resolve-Path -Relative)
#Remove the fullstop created by the relative resolve path.

Foreach ($FileSource in $FileSources) {
#Testing to see if the obeject is folder or file.

$fileSource1 = ($fileSource.trimstart(". "))

"Testing " + $SourceDirectory+$fileSource1 + "to see if type folder" | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII
$sourcepath = join-path $sourceDirectory $FileSource1
if (Test-Path -Path $sourcepath -PathType container) {
"Yup: This is a folder." | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII
}
Else {
"Nope: This is a file" | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII
}

#Does the source file or folder exist in the destination directory
$destinationpath = join-path $DestinationDirectory $FileSource1
$destinationpath | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII
if ((Test-Path -path $Destinationpath)){
"Object exists in destination folder" | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII
}
Else {
"Object does not exist in the destination folder." | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII
Copy-Item $sourcepath -Destination $Destinationpath
if ((Test-Path -path $Destinationpath)){
"Now Object exists in destination folder" | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII
}
}
}

$TimeEnd = get-date
write-Host "Finished analysing the Files."
"Finished analysing the Files." + $TimeEnd | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII
"Time Taken: " + ($TimeEnd - $TimeStart) | Out-File -FilePath $ErrorMessagesFile -append -Encoding ASCII


