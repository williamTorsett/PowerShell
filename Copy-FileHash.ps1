# Taken from http://wragg.io/a-powershell-cmdlet-to-copy-files-based-on-hash-difference/

#Hashing Algorith
$Algorithm = 'SHA256'

#Source Path
$Path = "C:\temp"
$SourcePath = (Resolve-Path -Path $Path).Path
write-host $SourcePath



#Destniation Path
$Destination = "C:\temp\"
Write-Host "Writing Destination Folder:" $Destination


If (-Not (Test-Path $Destination)){  
    New-Item -Path $Destination -ItemType Container | Out-Null
    Write-Warning "$Destination did not exist and has been created as a folder path."
}

$Destination = Join-Path ((Resolve-Path -Path $Destination).Path) -ChildPath '/'
write-host $Destination


$SourceFiles = (Get-ChildItem -Path $Source -Recurse:$Recurse -File).FullName

ForEach ($Source in $SourcePath) {  
    $SourceFiles = (Get-ChildItem -Path $Source -Recurse -File).FullName

    ForEach ($SourceFile in $SourceFiles) {
        $DestFile = Join-Path (Split-Path -Parent $SourceFile) -ChildPath '/'
        Write-Host $DestFile
        $DestFile = $DestFile -Replace "^$([Regex]::Escape($Source))", $Destination
        $DestFile = Join-Path -Path $DestFile -ChildPath (Split-Path -Leaf $SourceFile)

        $SourceHash = (Get-FileHash $SourceFile -Algorithm $Algorithm).hash

        If (Test-Path $DestFile) {
            $DestHash = (Get-FileHash $DestFile -Algorithm $Algorithm).hash
        }
        Else {
#        #The next If statement only works for powershell 6.
#            If ($PSCmdlet.ShouldProcess($DestFile, 'New-Item')) {
#                New-Item -Path $DestFile -Value (Get-Date).Ticks -Force | Out-Null
#                Write-Host $DestFile
#            }
            $DestHash = $null
        }

        If (($SourceHash -ne $DestHash) -and $PSCmdlet.ShouldProcess($SourceFile, 'Copy-Item')) {
            Copy-Item -Path $SourceFile -Destination $DestFile -Force:$Force -PassThru:$PassThru
        }
    }
}