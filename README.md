# Copy_Files

Copy all files from source path to destination path using Robocopy

To run a Powershell Script (in this case PowershellFileCopy.ps1) from a Windows 10 latpop using an account with administrative priveleges:

`powershell –ExecutionPolicy Bypass -file PowershellFileCopy.ps1`

To run silently in the background you can run something like this.

`powershell.exe -executionpolicy bypass -windowstyle hidden -noninteractive -nologo -file PowershellFileCopy.ps1`
