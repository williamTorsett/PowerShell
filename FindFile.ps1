#Created 8 December 2018
#This script is designed to copy one folder to a new folder in a different directory.
#This commmand can be put  at the top of every single script so that you don't have to check $?.
#This makes the code vastly simpler and more reliable.

$ErrorActionPreference = 'Stop'
$TimeStart = get-date

$SourceDirectory = "C:\Temp"
$DestinationDirectory = "C:\Temp1"
set-location $SourceDirectory

$FileSources = @()
$FileSources = @((Get-ChildItem -Path $SourceDirectory -Force -Recurse) | Resolve-Path -Relative)
#Remove the fullstop created by the relative resolve path.
foreach ($FileSource in $FileSources) {

}

Foreach ($FileSource in $FileSources) {
#Testing to see if the obeject is folder or file.

$fileSource1 = ($fileSource.trimstart(". "))

Write-host "Testing " $SourceDirectory$fileSource1 "to see if type folder"
$sourcepath = join-path $sourceDirectory $FileSource1
if (Test-Path -Path $sourcepath -PathType container) {
write-host "Yup: This is a folder."
}
Else {
write-host "Nope: This is a file"
}

#Does the source file or folder exist in the destination directory
$destinationpath = join-path $DestinationDirectory $FileSource1
write-host $destinationpath
if ((Test-Path -path $Destinationpath)){
Write-host "Object exists in destination folder"
}
Else {
write-host "Object does not exist in the destination folder."
Copy-Item $sourcepath -Destination $Destinationpath
if ((Test-Path -path $Destinationpath)){
Write-host "Now Object exists in destination folder"
}
}
}


