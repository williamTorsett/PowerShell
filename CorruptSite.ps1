Add-PSSnapin Microsoft.SharePoint.PowerShell
$CorruptSites = "https://nocsoc.sharepoint.com/sites/FulcrumPoCTeam"

Function DeleteCorruptSite
{
param($CorruptSite)
Write-Host $CorruptSite : -foreground red
$site = Get-SPSite $CorruptSite
$siteId = $site.Id
$siteDatabase = $site.ContentDatabase 
$siteDatabase.ForceDeleteSite($siteId, $false, $false)
Write-Host --Deleted from Content DB -foreground green
}
foreach ($CorruptSite in $CorruptSites)
{
 DeleteCorruptSite $CorruptSite
}